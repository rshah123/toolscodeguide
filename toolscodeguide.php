<?php
/*
Plugin name: Tools Code Guide
Plugin URI: http://www.agileinfoways.com/
Description: A widget to display title,Discription on your site
Version: 1.0.0
Author: Romesh Gupta
Author URI: http://www.agileinfoways.com
*/

// set permalink
		function set_permalink(){
			global $wp_rewrite;
			$wp_rewrite->set_permalink_structure('/%postname%/');
		}
		add_action('init', 'set_permalink');

/*Apply Css for Admin*/
		define( "TOOLS_CODE_GUIDE_URL", WP_PLUGIN_URL . '/ToolsCodeGuide/');
		define( "TOOLS_CODE_GUIDE_VERSION", "1.0.0");
		add_action( 'admin_init', 'tools_code_guide_admin_init' );
		function tools_code_guide_admin_init() {
		wp_enqueue_style('tools-code-guide-styles', TOOLS_CODE_GUIDE_URL.'toolscodeguide.css', false, TOOLS_CODE_GUIDE_VERSION);
		}

/*Install Datebase */
	function toolscodeguide_install() {
		global $wpdb;
		$table_name  = $wpdb->prefix . 'toolscodeguide';
		$table_name1 = $wpdb->prefix . 'toolshistory';
		$table_name2 = $wpdb->prefix . 'toolssetting';
		if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
			$sql =  "CREATE TABLE `$table_name` (
					`tool_id` int(11) unsigned NOT NULL auto_increment,
					`title` varchar(250) character set utf8 NOT NULL,
					`downloadurl` varchar(250) character set utf8 NOT NULL,
					`payment` varchar(250) character set utf8 NOT NULL,
					`ammount` varchar(250) character set utf8 NOT NULL,
					`status` varchar(250) character set utf8 NOT NULL,
					`discription` longtext character set utf8 NOT NULL,
					PRIMARY KEY  (`tool_id`)
				)";
		   $results = $wpdb->query($sql);
		}
		if($wpdb->get_var("SHOW TABLES LIKE '$table_name1'") != $table_name1) {
			$sql =  "CREATE TABLE `$table_name1` (
					`h_id` int(11) unsigned NOT NULL auto_increment,
					`tool_id` int(11) unsigned NOT NULL,
					`tool_title` varchar(250) character set utf8 NOT NULL,
					`ip_address` varchar(250) character set utf8 NOT NULL,
					`download_date` date NOT NULL,
					`email_address` varchar(250) character set utf8 NOT NULL,
					`tran_key` varchar(250) character set utf8 NOT NULL,
					`tran_ammount` varchar(250) character set utf8 NOT NULL,
					`tran_date` varchar(250) character set utf8 NOT NULL,
					`tran_ip_address` varchar(250) character set utf8 NOT NULL,
					`download` varchar(250) character set utf8 NOT NULL,
					`download_code` varchar(250) character set utf8 NOT NULL,
					PRIMARY KEY  (`h_id`)
				)";
		   $results = $wpdb->query($sql);
		}
		if($wpdb->get_var("SHOW TABLES LIKE '$table_name2'") != $table_name2) {
			$sql =  "CREATE TABLE `$table_name2` (
					`s_id` int(11) unsigned NOT NULL auto_increment,
					`api_username` varchar(250) character set utf8 NOT NULL,
					`api_key` varchar(250) character set utf8 NOT NULL,
					`api_password` varchar(250) character set utf8 NOT NULL,
					`business_id` varchar(250) character set utf8 NOT NULL,
					`payment_type` varchar(250) character set utf8 NOT NULL,
					`paypal_url` varchar(250) character set utf8 NOT NULL,
					`email` varchar(250) character set utf8 NOT NULL,
					`page_url` varchar(250) character set utf8 NOT NULL,
					PRIMARY KEY  (`s_id`)
				)";
		   $results = $wpdb->query($sql);
		   // add some test data
		   $data = array ('api_username' 	=> '',
			 				'api_key'  		=> '',
							'api_password'  => '',
							'business_id'   => '',
							'payment_type'  => 'Live',
							'paypal_url'  	=> 'https://www.paypal.com/',
							'email'  		=> 'romesh.gupta@agileinfoways.com',
							'page_url'		=> '' 
							);
		   $wpdb->insert($table_name2, $data);
		}
		}
	register_activation_hook(__FILE__,'toolscodeguide_install');

/* Admin Page Redirect */
	if(is_admin())
		include 'toolscodeguide_admin.php';
	
/*Widget Box display*/
	class toolscodeguide extends WP_Widget {
		function toolscodeguide() {
			$widget_ops = array('classname' => 'toolscodeguide');
			$this->WP_Widget('toolscodeguide', 'Tools Code Guide', $widget_ops);
		}
		function widget($args, $instance) {
			extract($args);
			$title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
			$snippet = $this->get_toolscodeguide();
			if($snippet!='') {
				echo $before_widget;
				if($title)
					echo $before_title.$title.$after_title;
					echo $after_widget;
			}
		}
		function get_toolscodeguide() {
	
	echo '<link rel="stylesheet" type="text/css" media="all" href=" '. plugins_url().'/ToolsCodeGuide/toolscodeguide.css"/>';
	
	?>
<script>
	function submitform(val){
		  document.forms["f-"+val].submit();
		}
	
	function submitfreeform(val){
		  document.forms["free-"+val].submit();
		}
	</script>
<?php
		if(isset($_POST['free']))
			{
				global $wpdb;
				$table_name1 = $wpdb->prefix .'toolshistory';
				$sqldata = array();
				// single record insert/update
				$sqldata['tool_id'] = $_POST['tool_id'];
				$sqldata['tool_title']=$_POST['title'];
				$sqldata['ip_address'] .= $_SERVER['REMOTE_ADDR'];
				$sqldata['download_date'] .= date('y-m-d');			
				$wpdb->insert($table_name1, $sqldata);
				
	/*download image code*/
	
	function remote_filesize($uri,$user='',$pw='') { 
		// start output buffering
		ob_start();
		// initialize curl with given uri
		$ch = curl_init($uri);
		// make sure we get the header
		curl_setopt($ch, CURLOPT_HEADER, 1);
		// make it a http HEAD request
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		// if auth is needed, do it here
		if (!empty($user) && !empty($pw))
		{
			$headers = array('Authorization: Basic ' .  base64_encode($user.':'.$pw)); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}
		$okay = curl_exec($ch);
		curl_close($ch);
		// get the output buffer
		$head = ob_get_contents();
		// clean the output buffer and return to previous
		// buffer settings
		ob_end_clean();
	   
		// gets you the numeric value from the Content-Length
		// field in the http header
		$regex = '/Content-Length:\s([0-9].+?)\s/';
		$count = preg_match($regex, $head, $matches);
	   
		// if there was a Content-Length field, its value
		// will now be in $matches[1]
		if (isset($matches[1]))
		{
			$size = $matches[1];
		}
		else
		{
			$size = 'unknown';
		}
	   
		return $size; }
	
	function downloadFile( $fullPath ){ 
	
	  // Must be fresh start
	  if( headers_sent() )
		die('Headers Sent');
	
	  // Required for some browsers
	  if(ini_get('zlib.output_compression'))
		ini_set('zlib.output_compression', 'Off');
	
	  // File Exists?
	 /* if( file_exists($fullPath) ){*/
	   
		// Parse Info / Get Extension
		$fsize = remote_filesize($fullPath);
		$path_parts = pathinfo($fullPath);
		
		$ext = strtolower($path_parts["extension"]);
		// Determine Content Type
		switch ($ext) {
		  case "pdf": $ctype="application/pdf"; break;
		  case "exe": $ctype="application/octet-stream"; break;
		  case "zip": $ctype="application/zip"; break;
		  case "doc": $ctype="application/msword"; break;
		  case "xls": $ctype="application/vnd.ms-excel"; break;
		  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
		  case "gif": $ctype="image/gif"; break;
		  case "png": $ctype="image/png"; break;
		  case "jpeg":
		  case "jpg": $ctype="image/jpg"; break;
		  default: $ctype="application/force-download";
		}
	
		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false); // required for certain browsers
		header("Content-Type: $ctype");
		header("Content-Disposition: attachment; filename=\"".basename($fullPath)."\";" );
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".$fsize);
		ob_clean();
		flush();
		readfile( $fullPath );
	
	 /* } else
		die('File Not Found');*/ }
	
	downloadFile($_POST['url']);
	exit;
	}
	
	if(isset($_POST['Submit']))
		{
				global $wpdb;
				$table_name2 = $wpdb->prefix .'toolshistory';
				$sqldata = array();
				$sqldata['tool_id']			=$_POST['tool_id']; 
				$sqldata['tool_title']		=$_POST['tool_title']; 	 	 	 	 	 	 
				$sqldata['email_address'] 	= $_REQUEST['email_address'];
				$sqldata['tran_key'] 		= $_REQUEST['tran_key'];
				$sqldata['tran_ammount'] 	= $_POST['ammount'];
				$sqldata['tran_date'] 		= date('y-m-d');
				$sqldata['tran_ip_address'] =$_SERVER['REMOTE_ADDR'];
				$sqldata['download'] 		= 'no';
				$sqldata['download_code'] 	= rand(111111,333333);
				
				// record insert
				
				$wpdb->insert($table_name2, $sqldata);
				
				$table_name5 = $wpdb->prefix .'toolssetting';
				$rows = $wpdb->get_results("SELECT * FROM $table_name5 where s_id = '1'");
				
				$token_id = $sqldata['download_code'];
				$h_id 	= $wpdb->insert_id;
				$tool_id = $_POST['tool_id'];
				$url =  $rows[0]->page_url.'?id='.base64_encode('h_id='.$h_id.'&tool_id='.$tool_id);
				echo $url; exit;
				$email = $_REQUEST['email_address'];
				$to      = $email;
				$subject = "Test mail";
				$message = 'Hello, your paypemt paid succesfully.  please check on below url: '.$url.' Your Token ID is : '.$token_id;
				$from    = "romesh@wordpress.com";
				// Always set content-type when sending HTML email
				$headers = "From:" . $from;
				
				wp_mail( $to, $subject, $message, $from, $headers); 
			}	
	
	if($_POST['paid']== 'paid')
		{
			global $wpdb, $toolscodeguide_adminurl;
			$table_name = $wpdb->prefix.'toolscodeguide';
			$pageURL = $toolscodeguide_adminurl;
			// now load the data to display
			$rows = $wpdb->get_results("SELECT * FROM $table_name where status= 'Active' and payment = 'Paid'");
			foreach($rows as $row) {
				if($row->tool_id == $_POST['tool_id']){
				 echo '<div class="tool_pay">
					   <h1>Payment Form</h1>
						<form  method="post" action="">
					
						<input type="hidden" id="tool_id" name="tool_id" value="'.$row->tool_id.'">
						<input type="hidden" id="tool_title" name="tool_title" value="'.$row->title.'">
						<input type="hidden" id="downloadurl" name="downloadurl" value="'.$row->downloadurl.'">
						<input type="hidden" id="ammount" name="ammount" value="'.$row->ammount.'">
							<p>
							 <div class="tool_title">Email Address&nbsp;:</div>
							 <input id="email_address" name="email_address" size="25"  minlength="2"/>
						   </p>
							<p>
							 <div class="tool_title">Transaction Key&nbsp;:</div>
							 <input id="tran_key" name="tran_key" size="25" minlength="2"/>
						   </p>
						   <p>
							 <div class="tool_title">Transaction Ammont&nbsp;:</div>
							'.$row->ammount.'
							</p>
						   <p>
						   <div class="tool_submit">
							 <input class="submit" type="submit" value="Submit" name="Submit" />
						   </div>
						   </p>
					 </form>
				  </div>';
				}
				
			}
		}
		else
		{
			global $wpdb, $toolscodeguide_adminurl;
			$table_name = $wpdb->prefix.'toolscodeguide';
			$pageURL = $toolscodeguide_adminurl;
			// now load the data to display
			$rows = $wpdb->get_results("SELECT * FROM $table_name where status = 'Active'");
				echo '<div class="tabular_div">
								<div class="table_header">
									<div class="col col1">Title</div>
									<div class="col col2">Download</div>
									<div class="col col3">Payment</div>
									<div class="col col4">Discription</div>    
								</div>';
			foreach($rows as $row) {
			if($row->payment =='Free'){
				$url = 	$row->downloadurl ;
				echo '<form method="post" id="free-'.$row->tool_id.'"  action="">
					<input type="hidden" id="tool_id" name="tool_id" value="'.$row->tool_id.'">
					<input type="hidden" id="url" name="url" value="'.$url.'">
					<input type="hidden" id="title" name="title" value="'.$row->title.'">
					<input type="hidden" id="free" name="free" value="free">';
					echo '<div class="table_content">
									<div class="col col1">'.$row->title.'</div>
									<div class="col col2"><a href="#" onclick="submitfreeform('.$row->tool_id.');">Download</a></div>
									<div class="col col3">'.$row->payment.'</div>
									<div class="col col4">'.$row->discription.'</div>
								</div>';
					echo '</form>';
				}
			else
				{
				echo '<form method="post" id="f-'.$row->tool_id.'"  action="">
				<input type="hidden" id="tool_id" name="tool_id" value="'.$row->tool_id.'">
				<input type="hidden" id="paid" name="paid" value="paid">';
				echo '<div class="table_content">
								<div class="col col1">'.$row->title.'</div>
								<div class="col col2"><a href="#" onclick="submitform('.$row->tool_id.');">Download</a></div>
								<div class="col col3">'.$row->payment.'</div>
								<div class="col col4">'.$row->discription.'</div>
							</div>
					</div>';
				echo '</form>';
				}
			}
		}
			
		}
	}
		function toolscodeguide_init() {
			register_widget('toolscodeguide');
		}
		add_action('widgets_init', 'toolscodeguide_init');

// Shortcode implementation
	function toolscodeguide_shortcode($attribs) {
		extract(shortcode_atts(array( ), $attribs));
		$tooldemo = new toolscodeguide;
		return $tooldemo->get_toolscodeguide();
}
add_shortcode('toolscodeguide', 'toolscodeguide_shortcode'); 

//token shortcode

/*Widget Box display*/
class token extends WP_Widget {
	function token() {
		$widget_ops = array('classname' => 'token');
		$this->WP_Widget('token', 'Token', $widget_ops);
	}
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
		$snippet = $this->get_token();
		if($snippet!='') {
			echo $before_widget;
			if($title)
				echo $before_title.$title.$after_title;
				echo $after_widget;
		}
	}
	function get_token() {
		
		?>
<script>
	function submittoken(val){
		
		 document.forms["token-"+val].submit();
	  }
</script>
<?php 
	if(isset($_POST['token']))
	{
		
		if($_POST['download'] == "no")
		
		{ 
			global $wpdb;
			$table_name1 = $wpdb->prefix .'toolshistory';
			$h_id = intval($_POST['h_id']);
			$sqldata = array();
			
			$sqldata['download_date'] = $_POST['download_date'];
			$sqldata['ip_address']    = $_SERVER['REMOTE_ADDR'];
			$sqldata['download'] = 'yes';
			
			
			if($h_id)
			{
				$wpdb->update($table_name1, $sqldata,array('h_id'=>$h_id));
			} 
			
	/*download image code*/


			function remote_filesize($uri,$user='',$pw='') { 
				// start output buffering
				ob_start();
				// initialize curl with given uri
				$ch = curl_init($uri);
				// make sure we get the header
				curl_setopt($ch, CURLOPT_HEADER, 1);
				// make it a http HEAD request
				curl_setopt($ch, CURLOPT_NOBODY, 1);
				// if auth is needed, do it here
				if (!empty($user) && !empty($pw))
				{
					$headers = array('Authorization: Basic ' .  base64_encode($user.':'.$pw)); 
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				}
				$okay = curl_exec($ch);
				curl_close($ch);
				// get the output buffer
				$head = ob_get_contents();
				// clean the output buffer and return to previous
				// buffer settings
				ob_end_clean();
			   
				// gets you the numeric value from the Content-Length
				// field in the http header
				$regex = '/Content-Length:\s([0-9].+?)\s/';
				$count = preg_match($regex, $head, $matches);
			   
				// if there was a Content-Length field, its value
				// will now be in $matches[1]
				if (isset($matches[1]))
				{
					$size = $matches[1];
				}
				else
				{
					$size = 'unknown';
				}
			   
				return $size; }
			function downloadFile( $fullPath ){ 
			
			  // Must be fresh start
			  if( headers_sent() )
				die('Headers Sent');
			
			  // Required for some browsers
			  if(ini_get('zlib.output_compression'))
				ini_set('zlib.output_compression', 'Off');
			
			  // File Exists?
			 /* if( file_exists($fullPath) ){*/
			   
				// Parse Info / Get Extension
				$fsize = remote_filesize($fullPath);
				$path_parts = pathinfo($fullPath);
				
				$ext = strtolower($path_parts["extension"]);
				// Determine Content Type
				switch ($ext) {
				  case "pdf": $ctype="application/pdf"; break;
				  case "exe": $ctype="application/octet-stream"; break;
				  case "zip": $ctype="application/zip"; break;
				  case "doc": $ctype="application/msword"; break;
				  case "xls": $ctype="application/vnd.ms-excel"; break;
				  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
				  case "gif": $ctype="image/gif"; break;
				  case "png": $ctype="image/png"; break;
				  case "jpeg":
				  case "jpg": $ctype="image/jpg"; break;
				  default: $ctype="application/force-download";
				}
			
				header("Pragma: public"); // required
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false); // required for certain browsers
				header("Content-Type: $ctype");
				header("Content-Disposition: attachment; filename=\"".basename($fullPath)."\";" );
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".$fsize);
				ob_clean();
				flush();
				readfile( $fullPath );
			
			 /* } else
				die('File Not Found');*/ }
			downloadFile($_POST['downloadurl']);
			exit;
	
		}

		else {echo "Your Download Code is Expired "; exit;	}
	}


if(isset($_POST['Submit']))
{
		global $wpdb;
 		$titlt_id = base64_decode($_REQUEST['id']);
		$pieces = explode("&", $titlt_id);
		$pieces[0];
		$pieces[1]; 
 	 		$h_id = explode("=",$pieces[0]);
	 		$tool_id = explode("=",$pieces[1]);
		
			$table_name = $wpdb->prefix .'toolshistory';
			$rows = $wpdb->get_results("SELECT * FROM $table_name where h_id = '$h_id[1]'");
			$rows['0']->download_code;
			$rows['0']->download;
			
			
			$table_name1 = $wpdb->prefix .'toolscodeguide';
			$rows1 = $wpdb->get_results("SELECT * FROM $table_name1 where tool_id = '$tool_id[1]'");
			$rows1['0']->downloadurl;
			 
		 
				
	if($_REQUEST['download'] ==$rows['0']->download_code )
		{
			
			
			echo '<form method="post" id="token-'.$tool_id[1].'"  action="">
			<input type="hidden" id="tool_id" name="tool_id" value="'.$tool_id[1].'">
			<input type="hidden" id="h_id" name="h_id" value="'.$h_id[1].'">
			<input type="hidden" id="token" name="token" value="token">
			<input type="hidden" id="download_date" name="download_date" value="'.date('y-m-d').'">
			<input type="hidden" id="download" name="download" value="'.$rows['0']->download.'">
			<input type="hidden" id="downloadurl" name="downloadurl" value="'.$rows1['0']->downloadurl.'">
			<div style="font-weight:bold;float:left;margin-right:10px;">Download Url:</div><a href="#" onclick="submittoken('.$tool_id[1].');">Download Plugin</a><br/>
				</form>';
			exit;
		} 
	
	else
	
		{
			echo '<div class="entry-content">
			
			<h1>Your Download Code is Incorrect, Please enter Right Download Code</h1>
			</div>';
			exit;
		}
	}


		echo '<form name="form1" method="post" action="">
				<h1 style="color:#F00">Please Download Plugin </h1>
				  <table  style="text-align:center">
					<tr>
					  <td>Token Number:</td>
					  <td>
					  <input type="text" name="download" placeholder="Enter Token Number"></td>
					</tr>
					<tr>
					  <td colspan="2" style="padding:10px 0px 0px 10px;"><input type="submit" name="Submit" id="Submit" value="Download"></td>
					</tr>
					
				  </table>
			</form>';
	}
}
	function token_init() {
  		register_widget('token');
	}
	add_action('widgets_init', 'token_init');
		// Shortcode implementation
		function token_shortcode($attribs) {
			extract(shortcode_atts(array( ), $attribs));
			$tooldemo = new token;
			return $tooldemo->get_token();
		}
	add_shortcode('token', 'token_shortcode');  	
?>
