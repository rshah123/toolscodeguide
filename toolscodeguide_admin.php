<?php 
/*Add  Option In Setting */
	add_action('admin_menu', 'toolscodeguide_menu');
	function toolscodeguide_menu() {
		add_menu_page( 'Tools Code Guide','Tools  Code Guide','administrator', 'toolscodeguide', 'toolscodeguide_options');
		add_submenu_page( 'toolscodeguide','Tools History Menu','Tools History Menu','administrator', 'toolshistory', 'toolshistory_options');
		add_submenu_page('toolscodeguide', 'Tools Setting','Tools Setting', 'administrator', 'toolssetting', 'toolssetting_options');
	}

/*Admin url*/
		$toolscodeguide_adminurl 	= admin_url().'options-general.php?page=toolscodeguide';
		$toolshistory_adminurl 		= admin_url().'options-general.php?page=toolshistory';
		$toolssetting_adminurl 		= admin_url().'options-general.php?page=toolssetting';

/*Tools Demo Options*/

	function toolscodeguide_options() {
		if($_POST) {
			toolscodeguide_save($_POST);
		}

		$action = isset($_GET['action']) ? $_GET['action'] : false;
		switch($action){
		case 'new' :
			toolscodeguide_edit();
			break;
			case 'new':
			toolscodeguide_edit();
			break;
		case 'edit':
			$tool_id = intval($_GET['tool_id']);
			toolscodeguide_edit($tool_id);
			break;
		case 'delete':
			$tool_id = intval($_GET['tool_id']);
			check_admin_referer('toolscodeguide_delete'.$tool_id);
			toolscodeguide_delete($tool_id);
			
			// now display summary page
			toolscodeguide_list();
			break;
			default:
			toolscodeguide_list();
		}
	}



/*Tools Histroy Options*/

	function toolshistory_options() {
		if($_POST) {
			toolshistory_save($_POST);
		}

		$action = isset($_GET['action']) ? $_GET['action'] : false;
		switch($action){
		case 'new':
			toolshistory_edit();
			break;
			case 'new':
			toolshistory_edit();
			break;
		case 'edit':
			$h_id = intval($_GET['h_id']);
			toolshistory_edit($h_id);
			break;
		case 'delete':
			$h_id = intval($_GET['h_id']);
			check_admin_referer('toolshistory_delete'.$h_id);
			toolshistory_delete($h_id);
			
			// now display summary page
			toolshistory_list();
			break;
			default:
			toolshistory_list();
		}
	}
	
	
	
/*Tools Setting Options*/

	function toolssetting_options() {
		if($_POST) {
			toolssetting_save($_POST);
		}

		$action = isset($_GET['action']) ? $_GET['action'] : false;
		switch($action){
		case 'edit':
			$s_id = intval($_GET['s_id']);
			toolssetting_edit($s_id);
			break;
			default:
			toolssetting_edit();
		}
	}
		
	
	
/* Tools Data Listing function*/

		function toolscodeguide_list() {
		global $wpdb, $toolscodeguide_adminurl;
		$table_name = $wpdb->prefix.'toolscodeguide';
		$pageURL = $toolscodeguide_adminurl;
		$where = '';
		
		// Display data Query
		
		 $rows = $wpdb->get_results("SELECT * FROM $table_name $where ORDER BY tool_id");
?>
            <div class="wrap">
           <?php  screen_icon();
echo '<h2>Tools Code Guide Menu</h2>'; ?>
            	<div class="tablenav">
  					<div class="alignleft actions">
    					<input type="submit" class="button-secondary action" id="toolscodeguide_add" name="toolscodeguide_add" value="Add New" onclick="location.href='options-general.php?page=toolscodeguide&action=new'"/>
  					</div>
				</div>
            <table class="widefat">
            <thead>
              <tr>
                <th>Tool Title</th>
                <th>Download URL</th>
                <th>Payment</th>
                <th>Ammount</th>
                <th>Status</th>
                <th>Discription</th>
                <th>Option</th>
              </tr>
            </thead>
            <tbody>
<?php 		
                    foreach($rows as $row) {
                        echo '<tr>
                        <td>'.$row->title.'</td>
                        <td>'.$row->downloadurl.'</td>
						<td>'.$row->payment.'</td>
						<td>'.$row->ammount.'</td>
						<td>'.$row->status.'</td>
						<td>'.$row->discription.'</td>
                        <td><a href="'.$pageURL.'&action=edit&tool_id='.$row->tool_id.'">Edit</a><br/>';
                        $del_link = wp_nonce_url($pageURL.$del_paged.'&action=delete&tool_id='.$row->tool_id, 'toolscodeguide_delete' . $row->tool_id);
                        echo '<a onclick="if ( confirm(\'You are about to delete Tool Code #'.$row->title.'\n Cancel to stop, OK to delete.\') ) { return true;}return false;" href="'.$del_link.'" title="Delete this post" class="submitdelete">Delete</a>';
                        echo  '</td></tr>';		
                    }
                    	echo '</tbody></table>';
                		echo '</div>';
         }

/* Tools History Listing function*/
function toolshistory_list() {
		global $wpdb, $toolshistory_adminurl;
		$table_name = $wpdb->prefix.'toolshistory';
		$pageURL = $toolshistory_adminurl;
		$where = '';
		
		// Display data Query
		
		 $rows = $wpdb->get_results("SELECT * FROM $table_name $where ORDER BY h_id");
?>
            <div class="wrap">
             <?php  screen_icon();
echo '<h2>Tools History Menu</h2>'; ?>
            	
            <table class="widefat">
            <thead>
              <tr>
                <th>Tool Title</th>
                <th>Download Ip</th>
                <th>Download Date</th>
                <th>Email Id</th>
                <th>Tran Key</th>
                <th>Tran Ammount</th>
                <th>Tran Date</th>
                <th>Tran Ip</th>
                <th>Download</th>
                <th>Option</th>
              </tr>
            </thead>
            <tbody>
<?php 		
                    foreach($rows as $row) {
                        echo '<tr>
                        <td>'.$row->tool_title.'</td>
                        <td>'.$row->ip_address.'</td>
						<td>'.$row->date.'</td>
						<td>'.$row->email_address.'</td>
						<td>'.$row->tran_key.'</td>
						<td>'.$row->tran_ammount.'</td>
						<td>'.$row->tran_date.'</td>
						<td>'.$row->tran_ip_address.'</td>
						<td>'.$row->download.'</td>
						<td>';
						 $del_link = wp_nonce_url($pageURL.$del_paged.'&action=delete&h_id='.$row->h_id, 'toolshistory_delete' . $row->h_id);
                        echo '<a onclick="if ( confirm(\'You are about to delete Tool Title : '.$row->tool_title.'\n Cancel to stop, OK to delete.\') ) { return true;}return false;" href="'.$del_link.'" title="Delete this post" class="submitdelete">Delete</a>';
                        echo  '</td></tr>';		
                    }
                    	echo '</tbody></table>';
                		echo '</div>';
         }


/* Delete function */
/*Toos code Guide*/
	function toolscodeguide_delete($tool_id){
			global $wpdb;
			$table_name = $wpdb->prefix .'toolscodeguide';
			$tool_id = intval($tool_id);
			$sql = "DELETE FROM $table_name WHERE tool_id = $tool_id";
			$wpdb->query($sql);
		}
		
/*Tools History*/		
		function toolshistory_delete($h_id){
			global $wpdb;
			$table_name = $wpdb->prefix .'toolshistory';
			$h_id = intval($h_id);
			$sql = "DELETE FROM $table_name WHERE h_id = $h_id";
			$wpdb->query($sql);
		}
		
/* Tools Data Save */

		function toolscodeguide_save($data) {
			global $wpdb;
			$table_name = $wpdb->prefix .'toolscodeguide';
			$tool_id = intval($data['tool_id']);
			check_admin_referer('toolscodeguide_edit'.$tool_id);
			$sqldata = array();
			// single record insert/update
			
			$sqldata['title'] = trim(stripslashes($data['tool_title']));
			$sqldata['downloadurl'] .= trim(stripslashes($data['tool_downloadurl']));
			$sqldata['payment'] .= trim(stripslashes($data['tool_payment']));
			$sqldata['ammount'] .= trim(stripslashes($data['tool_ammount']));
			$sqldata['status'] .= trim(stripslashes($data['tool_status']));
			$sqldata['discription'] .= trim(stripslashes($data['tool_discription']));
			
			if($tool_id)
					$wpdb->update($table_name, $sqldata, array('tool_id'=>$tool_id));
			else
					$wpdb->insert($table_name, $sqldata);
					wp_redirect('options-general.php?page=toolscodeguide');
			}
/*Edit Tools Code guide  function*/
	function toolscodeguide_edit($tool_id=0) {
	
		echo '<div class="wrap">';
		$title = '- Add New';
		
		if($tool_id) {
		$title = '- Edit';
		global $wpdb;
		$table_name = $wpdb->prefix . 'toolscodeguide';
		$sql = "SELECT * from $table_name where tool_id=$tool_id";
		$row = $wpdb->get_row($sql);
		}
	if($tool_id && !$row) {
		echo '<h3>The requested entry was not found.</h3>';
	} else {
	// display the add/edit form 
	global $toolscodeguide_adminurl;
?>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
  	<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
  		<script>
   			$(document).ready(function(){
    		$("#commentForm").validate();
  		});
  		</script>
        
        <script>
		function check(val)		
		{
			/*if(val == "Paid")
			{
				document.getElementById('mycheckbox').style.display = 'block';
			}
			if(val == "Free")
			{
				document.getElementById('mycheckbox').style.display = 'none';
			}*/
			
		}
		</script>
<script type="text/javascript" src="../wp-content/plugins/ToolsCodeGuide/ckeditor/ckeditor.js"></script>
<script>   
    $(function() { // Shorthand for $(document).ready(function() {
          $('#select_input').change(function() {
                $('#div').toggle($(this).val() == 'Paid');
          });
          $('#div').show();
    });
</script>

<?php
screen_icon();
echo '<h2>Tool Code Guide</h2>';
	echo '<form method="post" id="commentForm" action="'.$toolscodeguide_adminurl.'">
		'.wp_nonce_field('toolscodeguide_edit'.$tool_id).'
		<input type="hidden" tool_id="tool_id" name="tool_id" value="'.$tool_id.'">
				
				<div class="lab1">Tool Title:</div>
				<input name="tool_title" id="lab1_input" class="required" value="'.apply_filters('format_to_edit',$row->title).'"/><br/>
				<div class="lab1">Url Download:</div>
				<input name="tool_downloadurl" id="lab1_input" class="required" value="'.apply_filters('format_to_edit',$row->downloadurl).'"/><br/>
				<div class="lab1">Payment:</div>
				<select name="tool_payment" id="select_input" class="required" >
				<option value="">Selected</option>
				<option value="Paid"';				
				if($row->payment=='Paid')
				{
					echo "selected=selected";
				}
				echo '>Paid</option>
				<option value="Free"';
				if($row->payment=='Free')
				{
					echo "selected=selected";
				}
				echo '>Free</option>
				</select><br/>
				<div class="lab1">Status:</div>
				<div id="div" style=" margin: -25px 140px 0; float:left;">
				<input type="text" name="tool_ammount" id="mycheckbox" class="required" value="'.apply_filters('format_to_edit',$row->ammount).'" "/>
				</div>
				<select name="tool_status" id="select_input" class="required">
				<option value="">Selected</option>
				<option value="Active"';
				if($row->status=='Active')
				{
				 echo "selected=selected";	
				}
				echo'>Active</option>
				<option value="InActive"';
				if($row->status=='InActive')
				{
				 	echo "selected=selected";	
				}
				echo '>InActive</option>
				</select>
				<br/>
				<div class="lab1">Discription:</div>
				<textarea name="tool_discription" class="required">'.apply_filters('format_to_edit',$row->discription).'</textarea>';?>
				<script type="text/javascript">
			//<![CDATA[
				CKEDITOR.replace( 'tool_discription',
					{
						fullPage : true,
						extraPlugins : 'docprops'
					});
			//]]>
			</script> 
            
				<?php echo '</div>';
		
		echo '<div class="submit">
			<input class="button-primary" type="submit" name="toolscodeguide_Save" value="Save Changes" />
			</div>
			</form>';
	}
  echo '</div>';
}

/*Edit Tools setting*/

	function toolssetting_edit() {
		echo '<div class="wrap">';
		global $wpdb;
		$table_name2 = $wpdb->prefix . 'toolssetting';
		$sql = "SELECT * from $table_name2 where s_id='1'";
		$row = $wpdb->get_row($sql);
		if($s_id && !$row) {
		echo '<h3>The requested entry was not found.</h3>';
		} else {
		global $toolssetting_adminurl;
	
		screen_icon();
		echo '<h2>Tools Setting</h2>';
		echo '<form method="post" action="'.$toolssetting_adminurl.'">
		'.wp_nonce_field('toolssetting_edit'.$s_id).'
				<input type="hidden" id="s_id" name="s_id" value="'.$s_id.'">
		 	 	<div class="lab1">API Username:</div>
				<input name="api_username" id="lab1_input" value="'.apply_filters('format_to_edit',$row->api_username).'"/><br/>
				<div class="lab1">API Key:</div>
				<input name="api_key" id="lab1_input" value="'.apply_filters('format_to_edit',$row->api_key).'"/><br/>
				<div class="lab1">API Password:</div>
				<input name="api_password" id="lab1_input" value="'.apply_filters('format_to_edit',$row->api_password).'"/><br/>
				<div class="lab1">Bussines ID:</div>
				<input name="business_id" id="lab1_input" value="'.apply_filters('format_to_edit',$row->business_id).'"/><br/>
				<div class="lab1">Payment Tyepe:</div>
				<input name="payment_type" id="lab1_input" value="'.apply_filters('format_to_edit',$row->payment_type).'"/><br/>
				<div class="lab1">Paypal Url:</div>
				<input name="paypal_url" id="lab1_input" value="'.apply_filters('format_to_edit',$row->paypal_url).'"/><br/>
				<div class="lab1">Paypal Email:</div>
				<input name="email" id="lab1_input" value="'.apply_filters('format_to_edit',$row->email).'"/><br/>
				<div class="lab1">Page Url:</div>
				<input name="page_url" id="lab1_input" value="'.apply_filters('format_to_edit',$row->page_url).'"/>';
				echo '</div>';
		
				echo '<div class="submit">
				<input class="button-primary" type="submit" name="toolssetting_save" value="Update" />
				</div>
				</form>';
	}
  		echo '</div>';
  
}
/* Tools Setting Save */
		function toolssetting_save($data) {
			global $wpdb;
			$table_name2 = $wpdb->prefix .'toolssetting';
			$s_id = intval($data['s_id']);
					
			$sqldata = array();
			// single record insert/update
			 	 	 	 	 	 	 
			$sqldata['api_username'] = trim(stripslashes($data['api_username']));
			$sqldata['api_key'] .= trim(stripslashes($data['api_key']));
			$sqldata['api_password'] .= trim(stripslashes($data['api_password']));
			$sqldata['business_id'] .= trim(stripslashes($data['business_id']));
			$sqldata['payment_type'] .= trim(stripslashes($data['payment_type']));
			$sqldata['paypal_url'] .= trim(stripslashes($data['paypal_url']));
			$sqldata['email'] .= trim(stripslashes($data['email']));
			$sqldata['page_url'] .= trim(stripslashes($data['page_url']));
			$wpdb->update($table_name2, $sqldata, array('s_id'=>'1'));
		}
?>